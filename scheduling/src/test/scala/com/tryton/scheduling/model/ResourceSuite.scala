package com.tryton.scheduling.model

import org.scalatest.FunSuite

class ResourceSuite extends FunSuite {

  test("resourceShouldHaveNoTaskToDo") {
    //given
    val resource = new Resource(1, "resourceName")

    //when - then
    assert(resource.taskCount == 0)
    assert(resource.isTaskListEmpty)
    assert(resource.timeWhenResourceIsFree == 0)
  }

  test("resourceHasOneTaskToDo") {
    //given
    val startTime = 0
    val executionTime = 10
    val task = new Task(1, 1, "task1", executionTime)
    val resource = new Resource(1, "resourceName")

    //when
    resource.addTask(startTime, task)

    //then
    assert(resource.taskCount == 1)
    assert(!resource.isTaskListEmpty)
    assert(resource.timeWhenResourceIsFree == startTime + executionTime)
  }

  test("resourceHasTasksTasksToDo") {
    //given
    val startTime1 = 0
    val executionTime1 = 10
    val task1 = new Task(1, 1, "task1", executionTime1)

    val startTime2 = 12
    val executionTime2 = 20
    val task2 = new Task(1, 2, "task2", executionTime2)

    val resource = new Resource(1, "resourceName")

    //when
    resource.addTask(startTime1, task1)
    resource.addTask(startTime2, task2)

    //then
    assert(resource.taskCount == 2)
    assert(!resource.isTaskListEmpty)
    assert(resource.timeWhenResourceIsFree == startTime2 + executionTime2)
  }

  test("shouldThrowExceptionDueToInvalidStartTime") {
    //given
    val startTime1 = 0
    val executionTime1 = 10
    val task1 = new Task(1, 1, "task1", executionTime1)

    val startTime2 = 5
    val executionTime2 = 20
    val task2 = new Task(1, 2, "task2", executionTime2)

    val resource = new Resource(1, "resourceName")

    //when
    resource.addTask(startTime1, task1)
    val caught = intercept[IllegalArgumentException]{
      resource.addTask(startTime2, task2)
    }

    //then
    assert(caught.getMessage == "Cannot start task before: 10" )
    assert(resource.taskCount == 1)
    assert(!resource.isTaskListEmpty)
  }

  test("shouldReturnZeroDueToNoTaskSpecifiedForGivenJob") {
    //given
    val startTime1 = 0
    val executionTime1 = 10
    val jobId = 1
    val task1 = new Task(jobId, 1, "task1", executionTime1)

    val resource = new Resource(jobId, "resourceName")
    resource.addTask(startTime1, task1)

    //when
    val earliestBeginningTime: Int = resource.getEarliestStartTimeForJob(jobId + 1)

    //then
    assert(earliestBeginningTime == 0)
  }

  test("shouldReturnStartingTimeAndExecutionTimeOfSecondTask") {
    //given
    val startTime1 = 0
    val executionTime1 = 10
    val jobId1 = 1
    val task1 = new Task(jobId1, 1, "task1", executionTime1)

    val startTime2 = 15
    val executionTime2 = 20
    val task2 = new Task(jobId1, 2, "task2", executionTime2)

    val resource = new Resource(jobId1, "resourceName")
    resource.addTask(startTime1, task1)
    resource.addTask(startTime2, task2)

    //when
    val earliestBeginningTime = resource.getEarliestStartTimeForJob(jobId1)

    //then
    assert(earliestBeginningTime == startTime2 + executionTime2)
  }

  test("testOfGetEarliestBeginningTimeWhenSeveralJobsAreToDo") {
    //given
    val jobId1 = 1
    val jobId2 = 2
    val jobId3 = 3

    val startTime11 = 0
    val executionTime11 = 10
    val task11 = new Task(jobId1, 1, "task1", executionTime11)

    val startTime21 = 15
    val executionTime21 = 5
    val task21 = new Task(jobId2, 1, "task1", executionTime21)

    val startTime12 = 20
    val executionTime12 = 20
    val task12 = new Task(jobId1, 2, "task2", executionTime12)

    val startTime31 = 41
    val executionTime31 = 3
    val task31 = new Task(jobId3, 1, "task1", executionTime31)

    val startTime32 = 44
    val executionTime32 = 1
    val task32 = new Task(jobId3, 2, "task2", executionTime32)

    val startTime22 = 45
    val executionTime22 = 20
    val task22 = new Task(jobId2, 2, "task2", executionTime22)

    val resource = new Resource(1, "resourceName")
    resource.addTask(startTime11, task11)
    resource.addTask(startTime12, task12)
    resource.addTask(startTime21, task21)
    resource.addTask(startTime31, task31)
    resource.addTask(startTime32, task32)
    resource.addTask(startTime22, task22)

    //when - then
    assert(resource.getEarliestStartTimeForJob(jobId1) == startTime12 + executionTime12)
    assert(resource.getEarliestStartTimeForJob(jobId2) == startTime22 + executionTime22)
    assert(resource.getEarliestStartTimeForJob(jobId3) == startTime32 + executionTime32)
  }

  test("shouldThrowExceptionDueToToSmallGapInScheduleForTask") {
    //given
    val jobId1 = 1
    val jobId2 = 2

    val startTime11 = 0
    val executionTime11 = 10
    val task11 = new Task(jobId1, 1, "task1", executionTime11)

    val startTime21 = 15
    val executionTime21 = 6
    val task21 = new Task(jobId2, 1, "task1", executionTime21)

    val startTime12 = 20
    val executionTime12 = 20
    val task12 = new Task(jobId1, 2, "task2", executionTime12)

    val resource = new Resource(1, "resourceName")
    resource.addTask(startTime11, task11)
    resource.addTask(startTime12, task12)

    //when
    val caught = intercept[IllegalArgumentException]{
      resource.addTask(startTime21, task21)
    }

    //then
    assert(caught.getMessage == "Cannot start task at: " + startTime21 + ". Gap in schedule is too small")
  }

  test("shouldThrowExceptionDueToToSmallGapInScheduleForTask2") {
    //given
    val jobId1 = 1
    val jobId2 = 2
    val jobId3 = 3

    val startTime11 = 0
    val executionTime11 = 10
    val task11 = new Task(jobId1, 1, "task1", executionTime11)

    val startTime12 = 10
    val executionTime12 = 10
    val task12 = new Task(jobId1, 2, "task2", executionTime12)

    val startTime31 = 20
    val executionTime31 = 5
    val task31 = new Task(jobId3, 1, "task1", executionTime31)

    val startTime13 = 25
    val executionTime13 = 5
    val task13 = new Task(jobId1, 3, "task2", executionTime13)

    val startTime32 = 35
    val executionTime32 = 5
    val task32 = new Task(jobId3, 2, "task2", executionTime32)

    val startTime21 = 33
    val executionTime21 = 3
    val task21 = new Task(jobId2, 1, "task1", executionTime21)

    val resource = new Resource(1, "resourceName")
    resource.addTask(startTime11, task11)
    resource.addTask(startTime12, task12)
    resource.addTask(startTime31, task31)
    resource.addTask(startTime13, task13)
    resource.addTask(startTime32, task32)

    //when
    val caught = intercept[IllegalArgumentException]{
      resource.addTask(startTime21, task21)
    }

    //then
    assert(caught.getMessage == "Cannot start task at: " + startTime21 + ". Gap in schedule is too small")
  }

  test("shouldFindStartTimeBeforeFirstTaskInSchedule") {
    //given
    val jobId1 = 1
    val jobId2 = 2

    val startTime11 = 5
    val executionTime11 = 10
    val task11 = new Task(jobId1, 1, "task1", executionTime11)

    val executionTime21 = 5
    val task21 = new Task(jobId2, 1, "task1", executionTime21)

    val startTime12 = 20
    val executionTime12 = 20
    val task12 = new Task(jobId1, 2, "task2", executionTime12)

    val resource = new Resource(1, "resourceName")
    resource.addTask(startTime11, task11)
    resource.addTask(startTime12, task12)

    //when - then
    assert(resource.getEarliestStartTimeForTask(task21) == 0)
  }

  test("shouldFindStartTimeBeforeFirstTaskInSchedule2") {
    //given
    val jobId1 = 1
    val jobId2 = 2
    val jobId3 = 3

    val startTime11 = 5
    val executionTime11 = 5
    val task11 = new Task(jobId1, 1, "task1", executionTime11)

    val startTime12 = 10
    val executionTime12 = 10
    val task12 = new Task(jobId1, 2, "task2", executionTime12)

    val startTime31 = 20
    val executionTime31 = 5
    val task31 = new Task(jobId3, 1, "task1", executionTime31)

    val startTime13 = 25
    val executionTime13 = 5
    val task13 = new Task(jobId1, 3, "task2", executionTime13)

    val startTime32 = 30
    val executionTime32 = 5
    val task32 = new Task(jobId3, 2, "task2", executionTime32)

    val executionTime21 = 5
    val task21 = new Task(jobId2, 1, "task1", executionTime21)

    val resource = new Resource(1, "resourceName")
    resource.addTask(startTime11, task11)
    resource.addTask(startTime12, task12)
    resource.addTask(startTime31, task31)
    resource.addTask(startTime13, task13)
    resource.addTask(startTime32, task32)

    //when - then
    assert(resource.getEarliestStartTimeForTask(task21) == 0)
  }

  test("shouldFindStartTimeBetweenTasksInSchedule") {
    //given
    val jobId1 = 1
    val jobId2 = 2

    val startTime11 = 0
    val executionTime11 = 10
    val task11 = new Task(jobId1, 1, "task1", executionTime11)

    val executionTime21 = 5
    val task21 = new Task(jobId2, 1, "task1", executionTime21)

    val startTime12 = 20
    val executionTime12 = 20
    val task12 = new Task(jobId1, 2, "task2", executionTime12)

    val resource = new Resource(1, "resourceName")
    resource.addTask(startTime11, task11)
    resource.addTask(startTime12, task12)

    //when - then
    assert(resource.getEarliestStartTimeForTask(task21) == startTime11 + executionTime11)
  }

  test("shouldFindStartTimeBetweenTasksInSchedule2") {
    //given
    val jobId1 = 1
    val jobId2 = 2

    val startTime11 = 0
    val executionTime11 = 10
    val task11 = new Task(jobId1, 1, "task1", executionTime11)

    val startTime12 = 10
    val executionTime12 = 10
    val task12 = new Task(jobId1, 2, "task2", executionTime12)

    val startTime13 = 25
    val executionTime13 = 5
    val task13 = new Task(jobId1, 3, "task2", executionTime13)

    val executionTime21 = 5
    val task21 = new Task(jobId2, 1, "task1", executionTime21)

    val resource = new Resource(1, "resourceName")
    resource.addTask(startTime11, task11)
    resource.addTask(startTime12, task12)
    resource.addTask(startTime13, task13)

    //when - then
    assert(resource.getEarliestStartTimeForTask(task21) == startTime12 + executionTime12)
  }

  test("shouldFindStartTimeBetweenTasksInSchedule3") {
    //given
    val jobId1 = 1
    val jobId2 = 2
    val jobId3 = 3

    val startTime11 = 0
    val executionTime11 = 10
    val task11 = new Task(jobId1, 1, "task1", executionTime11)

    val startTime12 = 10
    val executionTime12 = 10
    val task12 = new Task(jobId1, 2, "task2", executionTime12)

    val startTime31 = 20
    val executionTime31 = 5
    val task31 = new Task(jobId3, 1, "task1", executionTime31)

    val startTime13 = 25
    val executionTime13 = 5
    val task13 = new Task(jobId1, 3, "task2", executionTime13)

    val startTime32 = 35
    val executionTime32 = 5
    val task32 = new Task(jobId3, 2, "task2", executionTime32)

    val executionTime21 = 5
    val task21 = new Task(jobId2, 1, "task1", executionTime21)

    val resource = new Resource(1, "resourceName")
    resource.addTask(startTime11, task11)
    resource.addTask(startTime12, task12)
    resource.addTask(startTime31, task31)
    resource.addTask(startTime13, task13)
    resource.addTask(startTime32, task32)

    //when - then
    assert(resource.getEarliestStartTimeForTask(task21) == startTime13 + executionTime13)
  }

  test("shouldFindStartTimeAfterTasksInSchedule") {
    //given
    val jobId1 = 1
    val jobId2 = 2

    val startTime11 = 0
    val executionTime11 = 10
    val task11 = new Task(jobId1, 1, "task1", executionTime11)

    val executionTime21 = 5
    val task21 = new Task(jobId2, 1, "task1", executionTime21)

    val startTime12 = 13
    val executionTime12 = 20
    val task12 = new Task(jobId1, 2, "task2", executionTime12)

    val resource = new Resource(1, "resourceName")
    resource.addTask(startTime11, task11)
    resource.addTask(startTime12, task12)

    //when - then
    assert(resource.getEarliestStartTimeForTask(task21) == startTime12 + executionTime12)
  }

  test("shouldFindStartTimeAfterTasksInSchedule2") {
    //given
    val jobId1 = 1
    val jobId2 = 2
    val jobId3 = 3

    val startTime11 = 0
    val executionTime11 = 10
    val task11 = new Task(jobId1, 1, "task1", executionTime11)

    val startTime12 = 10
    val executionTime12 = 10
    val task12 = new Task(jobId1, 2, "task2", executionTime12)

    val startTime31 = 20
    val executionTime31 = 5
    val task31 = new Task(jobId3, 1, "task1", executionTime31)

    val startTime13 = 25
    val executionTime13 = 5
    val task13 = new Task(jobId1, 3, "task2", executionTime13)

    val startTime32 = 30
    val executionTime32 = 5
    val task32 = new Task(jobId3, 2, "task2", executionTime32)

    val executionTime21 = 5
    val task21 = new Task(jobId2, 1, "task1", executionTime21)

    val resource = new Resource(1, "resourceName")
    resource.addTask(startTime11, task11)
    resource.addTask(startTime12, task12)
    resource.addTask(startTime31, task31)
    resource.addTask(startTime13, task13)
    resource.addTask(startTime32, task32)

    //when - then
    assert(resource.getEarliestStartTimeForTask(task21) == startTime32 + executionTime32)
  }
}
