package com.tryton.scheduling.model

import com.tryton.scheduling.model.{Job, Task}
import org.scalatest.FunSuite

class JobSuite extends FunSuite {

  test("jobShouldContainsNoTask") {
    //given
    val job = new Job(1, "jobName")

    //when
    val isEmpty = job.hasNextTask

    //then
    assert(!isEmpty)
  }

  test("jobShouldContainsTasks") {
    //given
    val jobId = 1
    val job = new Job(jobId, "jobName")
    job.addTask(new Task(jobId, 1, "task1", 10))

    //when
    val hasTask = job.hasNextTask

    //then
    assert(hasTask)
  }

  test("shouldContainOneTask") {
    //given
    val jobId = 1
    val job = new Job(jobId, "jobName")
    job.addTask(new Task(jobId, 1, "task1", 10))

    //when
    val count = job.taskCount

    //then
    assert(count == 1)
  }

  test("shouldReturnFirstTask") {
    //given
    val jobId = 1
    val job = new Job(jobId, "jobName")
    val expectedTask = new Task(jobId, 1, "task1", 10)
    job.addTask(expectedTask)

    //when
    val task = job.getNext

    //then
    assert(expectedTask == task)
    assert(!job.hasNextTask)
  }

  test("shouldReturnTasksInOrder") {
    //given
    val jobId = 1
    val job = new Job(jobId, "jobName")
    val expectedTask1 = new Task(jobId, 1, "task1", 10)
    val expectedTask2 = new Task(jobId, 2, "task2", 10)
    job.addTask(expectedTask1)
    job.addTask(expectedTask2)

    //when
    val task1 = job.getNext

    //then
    assert(expectedTask1 == task1)
    assert(job.hasNextTask)
    assert(job.taskCount == 1)

    //when
    val task2 = job.getNext

    //then
    assert(expectedTask2 == task2)
    assert(!job.hasNextTask)
    assert(job.taskCount == 0)
  }

  test("shouldThrowExceptionDueToGettingElementFromEmptyQueue") {
    //given
    val jobId = 1
    val job = new Job(jobId, "jobName")
    val expectedTask = new Task(jobId, 1, "task1", 10)
    job.addTask(expectedTask)

    //when
    job.getNext
    val caught = intercept[NoSuchElementException] {
      job.getNext
    }

    //then
    assert(caught.getMessage == "queue empty")
  }
}
