package com.tryton.scheduling.service

import com.tryton.scheduling.model.{Resource, Task}
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.FunSuite
import org.scalatest.mockito.MockitoSugar;

class ResourceSelectorSuite extends FunSuite with MockitoSugar {

  test("shouldReturnNoElement") {
    //given
    val task = createTask
    val resources = List[Resource]()

    //when
    val resourceSelector = new ResourceSelector()
    val selectedResource = resourceSelector.select(resources, task)

    //then
    assert(selectedResource.isEmpty)
  }

  test("shouldReturnGivenElement") {
    //given
    val task = createTask
    val expectedResource = prepareResourceMock(10)
    val resources = List[Resource](expectedResource)

    //when
    val resourceSelector = new ResourceSelector()
    val selectedResource = resourceSelector.select(resources, task)

    //then
    assert(selectedResource.get == expectedResource)
  }

  test("shouldReturnSecondGivenElement") {
    //given
    val task = createTask
    val resource1 = prepareResourceMock(10)
    val resource2 = prepareResourceMock(2)
    val resources = List[Resource](resource1, resource2)

    //when
    val resourceSelector = new ResourceSelector()
    val selectedResource = resourceSelector.select(resources, task)

    //then
    assert(selectedResource.get == resource2)
  }

  test("shouldReturnThirdGivenElement") {
    //given
    val task = createTask
    val resource1 = prepareResourceMock(10)
    val resource2 = prepareResourceMock(30)
    val resource3 = prepareResourceMock(2)
    val resource4 = prepareResourceMock(25)
    val resources = List[Resource](resource1, resource2, resource3, resource4)

    //when
    val resourceSelector = new ResourceSelector()
    val selectedResource = resourceSelector.select(resources, task)

    //then
    assert(selectedResource.get == resource3)
  }

  private def createTask: Task = {
    new Task(1, 1, "task1", 10)
  }

  private def prepareResourceMock(earliestStartTime: Int): Resource = {
    val resource = mock[Resource]
    when(resource.getEarliestStartTimeForTask(any(classOf[Task]))).thenReturn(earliestStartTime);
    resource
  }
}
