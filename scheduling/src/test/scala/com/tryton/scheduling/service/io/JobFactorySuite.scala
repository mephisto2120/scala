package com.tryton.scheduling.service.io

import com.tryton.scheduling.model.Job
import org.scalatest.FunSuite

class JobFactorySuite extends FunSuite {

  test("should create three jobs and each should contain four tasks") {
    //given
    val path = "src/test/resources/3x4/order.txt"
    val jobFactory = new JobFactory

    //when
    val jobs: Seq[Job] = jobFactory.create(path)

    //then
    assert(jobs.size == 3)
    assertTaskCount(jobs)
    assertJobExecutionTimes(jobs(0), 1, Array(6, 8, 9, 4))
    assertJobExecutionTimes(jobs(1), 2, Array(1, 3, 9, 6))
    assertJobExecutionTimes(jobs(2), 3, Array(5, 5, 3, 6))
  }

  private def assertTaskCount(jobs: Seq[Job]) {
    for (i <- jobs.indices) {
      assert(jobs(i).taskCount == 4)
    }
  }

  private def assertJobExecutionTimes(job: Job, expectedJobId: Int, expectedTimes: Seq[Int]) {
    assert(job.id == expectedJobId)
    assert(job.getNext.executionTime == expectedTimes(0))
    assert(job.getNext.executionTime == expectedTimes(1))
    assert(job.getNext.executionTime == expectedTimes(2))
    assert(job.getNext.executionTime == expectedTimes(3))
  }
}
