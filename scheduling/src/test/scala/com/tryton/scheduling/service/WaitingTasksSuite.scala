package com.tryton.scheduling.service

import com.tryton.scheduling.model.{Job, Task}
import org.scalatest.FunSuite

import scala.collection.mutable


class WaitingTasksSuite extends FunSuite {

  test("shouldContainNoTask") {
    //given
    val waitingTasks = new WaitingTasks(mutable.Map.empty)

    //when
    val waitingTasksList = waitingTasks.getTasksWaitingForSelection

    //then
    assert(waitingTasksList.isEmpty)
  }

  test("shouldReturnTasksFromJobs") {
    //given
    val jobId1 = 1
    val job1 = new Job(jobId1, "jobName1")
    val expectedTask11 = new Task(jobId1, 1, "task11", 10)
    val expectedTask12 = new Task(jobId1, 2, "task12", 10)
    job1.addTask(expectedTask11)
    job1.addTask(expectedTask12)

    val jobId12= 2
    val job2 = new Job(jobId12, "jobName2")
    val expectedTask21 = new Task(jobId12, 1, "task21", 20)
    val expectedTask22 = new Task(jobId12, 2, "task22", 20)
    job2.addTask(expectedTask21)
    job2.addTask(expectedTask22)

    val jobId13= 3
    val job3 = new Job(jobId13, "jobName3")
    val expectedTask31 = new Task(jobId13, 1, "task31", 30)
    val expectedTask32 = new Task(jobId13, 2, "task32", 30)
    job3.addTask(expectedTask31)
    job3.addTask(expectedTask32)


    val jobs: mutable.Map[Int, Job] = mutable.Map(job1.id -> job1, job2.id -> job2, job3.id -> job3)
    val waitingTasks = new WaitingTasks(jobs)

    // Step 1.
    //when
    val waitingTasksList1 = waitingTasks.getTasksWaitingForSelection

    //then
    assert(waitingTasksList1.size == 3)
    assert(waitingTasksList1.contains(expectedTask11))
    assert(waitingTasksList1.contains(expectedTask21))
    assert(waitingTasksList1.contains(expectedTask31))

    // Step 2.
    //when
    waitingTasks.select(expectedTask21)
    val waitingTasksList2 = waitingTasks.getTasksWaitingForSelection

    //then
    assert(waitingTasksList2.size == 3)
    assert(waitingTasksList2.contains(expectedTask11))
    assert(waitingTasksList2.contains(expectedTask22))
    assert(waitingTasksList2.contains(expectedTask31))

    // Step 3.
    //when
    waitingTasks.select(expectedTask22)
    val waitingTasksList3 = waitingTasks.getTasksWaitingForSelection

    //then
    assert(waitingTasksList3.size == 2)
    assert(waitingTasksList3.contains(expectedTask11))
    assert(waitingTasksList3.contains(expectedTask31))

    // Step 4.
    //when
    waitingTasks.select(expectedTask31)
    val waitingTasksList4 = waitingTasks.getTasksWaitingForSelection

    //then
    assert(waitingTasksList4.size == 2)
    assert(waitingTasksList4.contains(expectedTask11))
    assert(waitingTasksList4.contains(expectedTask32))

    // Step 5.
    //when
    waitingTasks.select(expectedTask11)
    val waitingTasksList5 = waitingTasks.getTasksWaitingForSelection

    //then
    assert(waitingTasksList5.size == 2)
    assert(waitingTasksList5.contains(expectedTask12))
    assert(waitingTasksList5.contains(expectedTask32))

    // Step 6.
    //when
    waitingTasks.select(expectedTask12)
    val waitingTasksList6 = waitingTasks.getTasksWaitingForSelection

    //then
    assert(waitingTasksList6.size == 1)
    assert(waitingTasksList6.contains(expectedTask32))

    // Step 7.
    //when
    waitingTasks.select(expectedTask32)
    val waitingTasksList7 = waitingTasks.getTasksWaitingForSelection

    //then
    assert(waitingTasksList7.isEmpty)
  }
}
