package com.tryton.scheduling.service.io

import com.tryton.scheduling.model.Resource
import org.scalatest.FunSuite

class ResourceFactorySuite extends FunSuite {

  test("should create four resources") {
    //given
    val path = "src/test/resources/3x4/resources.txt"
    val resourcesFactory = new ResourceFactory

    //when
    val resources: Seq[Resource] = resourcesFactory.create(path)

    //then
    assert(resources.size == 4)
    assertResource(resources(0), 1, "machine1")
    assertResource(resources(1), 2, "machine2")
    assertResource(resources(2), 3, "machine3")
    assertResource(resources(3), 4, "machine4")
  }

  private def assertResource(resource: Resource, expectedId: Int, expectedName: String) {
    assert(resource.id == expectedId)
    assert(resource.name == expectedName)
  }
}
