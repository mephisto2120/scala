package com.tryton.scheduling.service

import com.tryton.scheduling.model.Task
import org.scalatest.FunSuite

class LongestProcessingTimeSuite extends FunSuite {
  private val tasksSelection = new LongestProcessingTime()

  test("testSelectIfListIsEmpty") {
    //given
    val tasks = List[Task]()

    //when
    val selection: List[Task] = tasksSelection.select(tasks)

    //then
    assert(selection.isEmpty)
  }

  test("testSelectIfListIsOneElement") {
    //given
    val task1 = new Task(1, 1, "task1", 10)
    val tasks = List[Task](task1)

    //when
    val selection: List[Task] = tasksSelection.select(tasks)

    //then
    assert(selection.size == tasks.size)
    assert(selection.head == task1)
  }

  test("testSelectIfListIsMoreThantOneElement") {
    //given
    val jobId = 1;
    val task1 = new Task(jobId, 1, "task1", 10)
    val task2 = new Task(jobId, 2, "task2", 100)
    val task3 = new Task(jobId, 3, "task3", 1)
    val tasks = List[Task](task1, task2, task3)

    //when
    val selection: List[Task] = tasksSelection.select(tasks)

    //then
    assert(selection.size == 1)
    assert(selection.head == task2)
  }

  test("testSelectIfListIsMoreThantOneElementAndTwoHaveSameLongestExecutionTime") {
    //given
    val jobId = 1;
    val task1 = new Task(jobId, 1, "task1", 10)
    val task2 = new Task(jobId, 2, "task2", 100)
    val task3 = new Task(jobId, 3, "task3", 1)
    val task4 = new Task(jobId, 4, "task4", 100)
    val tasks = List[Task](task1, task2, task3, task4)

    //when
    val selection: List[Task] = tasksSelection.select(tasks)

    //then
    assert(selection.size == 2)
    assert(selection.head == task2)
    assert(selection(1) == task4)
  }
}