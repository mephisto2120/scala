package com.tryton.scheduling.model

class Task(val jodId: Int, val orderSequenceNumber: Int, val name: String, val executionTime: Int)