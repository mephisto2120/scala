package com.tryton.scheduling.model

import scala.collection.mutable

class Job(val id: Int, val name: String) {

  val tasks = new mutable.Queue[Task]

  def addTask(task: Task) {
    tasks += task
  }

  def getNext: Task = tasks.dequeue()

  def taskCount: Int = tasks.size

  def hasNextTask: Boolean = tasks.nonEmpty
}
