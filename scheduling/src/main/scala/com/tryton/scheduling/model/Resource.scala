package com.tryton.scheduling.model

import scala.collection.SortedMap
import scala.collection.immutable.TreeMap

class Resource(val id: Int, val name: String) {

  var selectedTasks: SortedMap[Int, Task] = TreeMap()

  var timeWhenResourceIsFree: Int = 0

  def addTask(startTime: Int, task: Task) {
    val earliestBeginningTimeForJob = getEarliestStartTimeForJob(task.jodId)
    if (startTime < earliestBeginningTimeForJob) {
      throw new IllegalArgumentException("Cannot start task before: " + earliestBeginningTimeForJob)
    }

    val earliestStartTime = findFirstGap(startTime, earliestBeginningTimeForJob, task)

    if (startTime < earliestStartTime) {
      throw new IllegalArgumentException("Cannot start task at: " + startTime + ". Gap in schedule is too small")
    }
    selectedTasks ++= Map(startTime -> task)
    if (startTime + task.executionTime > timeWhenResourceIsFree) {
      timeWhenResourceIsFree = startTime + task.executionTime
    }
  }

  def getTimeWhenResourceIsFree: Int = timeWhenResourceIsFree

  def taskCount: Int = selectedTasks.size

  def isTaskListEmpty: Boolean = selectedTasks.isEmpty

  def getEarliestStartTimeForTask(task: Task) = {
    val earliestBeginningTimeForJob = getEarliestStartTimeForJob(task.jodId)
    findFirstGap(0, earliestBeginningTimeForJob, task)
  }

  def getEarliestStartTimeForJob(jobId: Int) = {
    def durationTimeComparator(a: (Int, Task), b: (Int, Task)): Boolean = {
      getTaskFinishTime(a) > getTaskFinishTime(b)
    }
    def getTaskFinishTime(entry: (Int, Task)): Int = {
      entry._1 + entry._2.executionTime
    }
    def hasSameJobId(jobId: Int, entry: (Int, Task)): Boolean = {
      jobId == entry._2.jodId
    }

    val filtered = selectedTasks.filter(entry => hasSameJobId(jobId, entry))
    if (filtered.isEmpty) {
      0
    } else {
      val entry: (Int, Task) = {
        filtered.reduceLeft((a, b) =>
          if (durationTimeComparator(a, b)) a else b)
      }
      getTaskFinishTime(entry)
    }
  }

  private def findFirstGap(startTime: Int, earliestBeginningTimeForJob: Int, task: Task): Int = {
    val earliestStartTime = Math.max(earliestBeginningTimeForJob, startTime)
    val tasksAfterEarliestStartTime = selectedTasks.filter(startTimePredicate(earliestStartTime))


    def canBePlacedBeforeFirstFiltered: Boolean = {
      tasksAfterEarliestStartTime.isEmpty || earliestStartTime + task.executionTime <= tasksAfterEarliestStartTime.firstKey
    }

    if (canBePlacedBeforeFirstFiltered) {
      earliestStartTime
    } else {
      findGapInTasksAfterEarliestStartTime(tasksAfterEarliestStartTime, earliestStartTime, task.executionTime)
    }
  }

  private def startTimePredicate(earliestStartTime: Int): ((Int, Task)) => Boolean = {
    def isAfterEarliestStartTime(entry: (Int, Task)): Boolean = {
      entry._1 >= earliestStartTime
    }
    entry => isAfterEarliestStartTime(entry)
  }

  private def findGapInTasksAfterEarliestStartTime(tasksAfterEarliestStartTime: SortedMap[Int, Task],
                                                   searchStartTime: Int, executionTime: Int): Int = {
    def findEarliestStartTime(startTime: Int, task: Task): Int = {
      val nextStartTime = findNextStartTime(tasksAfterEarliestStartTime, startTime)
      val possibleStartTime = Math.max(searchStartTime, startTime)

      if (isNewTaskCanBePlacedBetween(possibleStartTime, task, executionTime, nextStartTime)) {
        possibleStartTime + task.executionTime
      } else if (isOnlyOneTaskAfter(tasksAfterEarliestStartTime)) {
        startTime + task.executionTime
      } else {
        calculateEarliestStartTimeWhenGapTooSmall(nextStartTime, tasksAfterEarliestStartTime)
      }
    }

    val map: Iterable[Int] = tasksAfterEarliestStartTime.map {
      case (startTime, task) => findEarliestStartTime(startTime, task)
    }

    map.reduceLeft((a, b) => if (a > b) a else b)
  }

  private def findNextStartTime(tasksAfterEarliestStartTime: SortedMap[Int, Task], startTime: Int): Int = {
    val elementsAfterStartTime = tasksAfterEarliestStartTime.from(startTime + 1)
    if (elementsAfterStartTime.isEmpty) -1 else elementsAfterStartTime.firstKey
  }

  private def isNewTaskCanBePlacedBetween(possibleStartTime: Int, task: Task, executionTime: Int, nextStartTime: Int): Boolean = {
    possibleStartTime + task.executionTime + executionTime <= nextStartTime
  }

  private def isOnlyOneTaskAfter(tasksAfterEarliestStartTime: SortedMap[Int, Task]): Boolean = {
    tasksAfterEarliestStartTime.size == 1
  }

  private def calculateEarliestStartTimeWhenGapTooSmall(nextStartTime: Int,
                                                        tasksAfterEarliestStartTime: SortedMap[Int, Task]): Int = {
    if (isTaskAfterInSchedule(nextStartTime)) {
      nextStartTime + getExecutionTime(tasksAfterEarliestStartTime.get(nextStartTime))
    } else 0
  }

  private def isTaskAfterInSchedule(nextStartTime: Int): Boolean = {
    nextStartTime != -1
  }

  private def getExecutionTime(taskOption: Option[Task]): Int = {
    taskOption match {
      case Some(scheduledTask) => scheduledTask.executionTime
      case None => 0
    }
  }
}