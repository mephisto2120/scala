package com.tryton.scheduling.service

import com.tryton.scheduling.model.Task

import scala.util.Random

class RandomTasksSelection extends TasksSelection {
  protected def selectTasks(tasks: List[Task]): List[Task] = {
    val random = new Random(System.currentTimeMillis)
    val index = random.nextInt(tasks.size)
    List(tasks(index))
  }
}
