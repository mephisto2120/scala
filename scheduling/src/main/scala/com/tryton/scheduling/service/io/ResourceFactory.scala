package com.tryton.scheduling.service.io

import com.tryton.scheduling.model.Resource

import scala.io.Source

class ResourceFactory {

  def create(path: String): Seq[Resource] = {
    for ((line, index) <- Source.fromFile(path).getLines().toSeq.zipWithIndex)
      yield createResource(index + 1, line)
  }

  private def createResource(resourceId: Int, line: String): Resource = {
    def getResourceName: String = {
      val parts = line.split(';')
      parts(0)
    }
    new Resource(resourceId, getResourceName)
  }
}
