package com.tryton.scheduling.service

import com.tryton.scheduling.model.{Resource, Task}


class ResourceSelector {
  def select(resources: List[Resource], task: Task): Option[Resource] = {
    resources match {
      case List() => Option.empty
      case _ => getResourceWithEarliestStartTimeForTask(resources, task)
    }
  }

  private def getResourceWithEarliestStartTimeForTask(resources: List[Resource], task: Task): Some[Resource] = {
    val resourcesMap: Map[Int, Resource] = resources.map(resourceMapper(task)).toMap
    Some(getResourceWithEarliestStartTime(resourcesMap))
  }

  private def resourceMapper(task: Task): (Resource) => (Int, Resource) = {
    resource => resource.getEarliestStartTimeForTask(task) -> resource
  }

  private def getResourceWithEarliestStartTime[Resource](map: Map[Int, Resource]): Resource = map(map.keys.min)
}
