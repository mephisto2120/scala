package com.tryton.scheduling.service

import com.tryton.scheduling.model.Task

class ShortestProcessingTime extends TasksSelection {
  protected def selectTasks(tasks: List[Task]): List[Task] = {
    val shortest = tasks.reduceLeft((a, b) => if (a.executionTime < b.executionTime) a else b)
    tasks.filter(t => t.executionTime == shortest.executionTime)
  }
}
