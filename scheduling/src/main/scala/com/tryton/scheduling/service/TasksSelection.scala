package com.tryton.scheduling.service

import com.tryton.scheduling.model.Task

trait TasksSelection {
  def select(tasks: List[Task]): List[Task] = {
    if (tasks.isEmpty) List.empty else selectTasks(tasks)
  }

  protected def selectTasks(tasks: List[Task]): List[Task]
}
