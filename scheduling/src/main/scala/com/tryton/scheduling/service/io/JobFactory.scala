package com.tryton.scheduling.service.io

import com.tryton.scheduling.model.{Job, Task}

import scala.io.Source

class JobFactory {

  def create(path: String): Seq[Job] = {
    for ((line, index) <- Source.fromFile(path).getLines().toSeq.zipWithIndex)
      yield createJob(index + 1, line)
  }

  private def createJob(jobId: Int, line: String): Job = {
    val job = new Job(jobId, "job" + jobId)
    addTasks(job, line)
    job
  }

  private def addTasks(job: Job, line: String) {
    val timeArray: Seq[Int] = getTasksExecutionTimes(line)
    for (i <- timeArray.indices) {
      val executionTime = timeArray(i)
      val task = new Task(job.id, i + 1, "task" + i + 1, executionTime)
      job.addTask(task)
    }
  }

  private def getTasksExecutionTimes(line: String): Seq[Int] = {
    val parts = line.split(';')
    parts(3).split('|').map(_.toInt)
  }
}
