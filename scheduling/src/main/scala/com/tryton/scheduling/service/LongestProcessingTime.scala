package com.tryton.scheduling.service

import com.tryton.scheduling.model.Task

class LongestProcessingTime extends TasksSelection {
  protected def selectTasks(tasks: List[Task]): List[Task] = {
    val longest = tasks.reduceLeft((a, b) => if (a.executionTime > b.executionTime) a else b)
    tasks.filter(t => t.executionTime == longest.executionTime)
  }
}
