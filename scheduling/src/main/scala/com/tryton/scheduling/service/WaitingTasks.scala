package com.tryton.scheduling.service

import com.tryton.scheduling.model.{Job, Task}

import scala.collection.mutable


class WaitingTasks(val jobs: mutable.Map[Int, Job]) {

  var waitingTasks: mutable.Map[Task, Int] = {
    val tasks: mutable.Map[Task, Int] = mutable.Map.empty
    jobs.foreach {
      case (key, value) => tasks += value.getNext -> key
    }
    tasks
  }

  def getTasksWaitingForSelection: List[Task] = waitingTasks.keys.toList

  def select(task: Task) = {

    def jobHasNextTask(jobOption: Option[Job]): Boolean = {
      jobOption match {
        case Some(job) => job.hasNextTask
        case None => false
      }
    }

    val jobIdOption: Option[Int] = waitingTasks.get(task)
    if (jobIdOption.isDefined) {
      waitingTasks.remove(task)

      val jobOption: Option[Job] = jobs.get(jobIdOption.get)
      if (jobHasNextTask(jobOption)) {
        waitingTasks += jobOption.get.getNext -> jobIdOption.get
      }
    }
  }
}
